// nombre tiene que ser una variable
// del tipo texto
function saludar( nombre:string ) {
    console.log("Hola " + nombre.toUpperCase());
}

var wolverine = {
    nombre: "James 'Logan' Howllet"
};

saludar(wolverine.nombre);


// Ejemplos
// Datos de tipo string
var Cyclope: string = "Scott Summers"
Cyclope = "Otro nombre"; // Es valido
Cyclope = 2 // No es valido

// Dato de tipo number
var age: number = 29;
age = 0xf00d; // Es hexadecimal y es correcto
age = '3'; // Es un string e incorrecto

// Dato de tipo boolean
var havePets: boolean = true;
havePets = false; // Es correcto
havePets = 3 // Es incorrecto

// Ejemplo
const test = name + havePets; // No se puede sumar number + boolean