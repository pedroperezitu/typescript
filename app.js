// nombre tiene que ser una variable
// del tipo texto
function saludar(nombre) {
    console.log("Hola " + nombre.toUpperCase());
}
var wolverine = {
    nombre: "James 'Logan' Howllet"
};
saludar(wolverine.nombre);
// Ejemplos
// Datos de tipo string
var Cyclope = "Scott Summers";
Cyclope = "Otro nombre"; // Es valido
Cyclope = 2; // No es valido
// Dato de tipo number
var age = 29;
age = 0xf00d; // Es hexadecimal y es correcto
age = '3'; // Es un string e incorrecto
// Dato de tipo boolean
var havePets = true;
havePets = false; // Es correcto
havePets = 3; // Es incorrecto
// Ejemplo
var test = name + havePets; // No se puede sumar number + boolean
//# sourceMappingURL=app.js.map